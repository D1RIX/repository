package com.app.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.Domain.Client;
import com.app.Repository.ClientRepository;
//defining the business logic
@Service
public class ClientService
{
    @Autowired
    ClientRepository clientRepository;
    //getting all clients records
    public List<Client> getAllClient()
    {
        List<Client> clients = new ArrayList<Client>();
        clientRepository.findAll().forEach(client -> clients.add(client));
        return clients;
    }
    //getting record
    public Client getClientById(int id)
    {
        return clientRepository.findById(id).get();
    }
    public void saveOrUpdate(Client client)
    {
        clientRepository.save(client);
    }
    //deleting record
    public void delete(int id)
    {
        clientRepository.deleteById(id);
    }
}